﻿using System;
using System.Collections.Generic;
using System.Linq;
using Marblets;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameForestTest
{
    public class GameBoard:DrawableGameComponent
    {
        private readonly Color[] _elementColors =
        { new Color(255, 0, 0), 
          new Color(40, 175, 255),
          new Color(40, 255, 20),
          new Color(255, 255, 0),
          new Color(255, 20, 230) };

        private Texture2D _marbleTexture;
        private Texture2D _cursorTexture;
        private Texture2D _selectedMarbleTexture;
        private bool _isMousePressed;
        private double _elapsedSeconds;

        public const int BoardSize = 8;

        private static readonly Random Random = new Random();
        /// <summary>
        /// 
        /// </summary>
        private readonly List<Element> _elements = new List<Element>();

        private readonly SpriteBatch _spriteBatch;
        private int _highScore;
        private List<Element> _selectedElements = new List<Element>();

        public GameBoard(Game game, SpriteBatch spriteBatch) : base(game)
        {
            _spriteBatch = spriteBatch;
        }

        public override void Update(GameTime gameTime)
        {
            if (GameStateService.Current.GameState != GameState.InGame)
            {
                return;
            }

            base.Update(gameTime);

            if (_elapsedSeconds < 0.00001)
            {
                return;
            }
            
            _elapsedSeconds -= gameTime.ElapsedGameTime.TotalSeconds;

            if (_elements.TrueForAll(e => e.AnimationIsEnded))
            {
                GameStateService.Current.StopAnimation();

                SwapBackIfNeed();

                // и здесь же меняем местами элементы обратно
                DestroyElements();

                // После удаления нужно другие элементы переместить вниз
                FallDownElements();

                // Меняем логику - теперь у нас создаются элементы на верхней строке
                CreateNewElements();
            }

            if (GameStateService.Current.IsAnimated)
            {
                return;
            }

            SetSelected();
            SwapElements();
        }

        private void SwapBackIfNeed()
        {
            if (_selectedElements.Count < 2)
            {
                return;
            }

            if (_selectedElements.Any(
                selectedElement =>
                    _elements.CanDestroyElements(selectedElement.X, selectedElement.Y, selectedElement.Color)))
            {
                _selectedElements.Clear();
                return;
            }
            _selectedElements[0].NextX = _selectedElements[1].X;
            _selectedElements[0].NextY = _selectedElements[1].Y;
            _selectedElements[1].NextX = _selectedElements[0].X;
            _selectedElements[1].NextY = _selectedElements[0].Y;

            _selectedElements.Clear();

            GameStateService.Current.StartAnimation();
        }

        private void CreateNewElements()
        {
            for (int i = 0; i < BoardSize; i++)
            {
                if (!_elements.IsElementExist(i, 0))
                {
                    var element = new Element(_marbleTexture, _selectedMarbleTexture, i, -1, _elementColors[Random.Next(_elementColors.GetLength(0))]);
                    element.NextY = 0;
                    _elements.Add(element);
                }
            }
        }

        private void FallDownElements()
        {
            // Бежим по оси Y снизу вверх, перемещая элементы на максимально доступный y
            // -2, потому что проверку на низ мы делаем с предпоследнего ряда
            for (int y = BoardSize - 2; y >= 0; y--)
            {
                for (int x = 0; x < BoardSize; x++)
                {
                    var element = _elements.GetElement(x, y);

                    if (element == null)
                    {
                        continue;
                    }

                    // Мы должны найти максимально возможную позицию для этого элемента по y
                    var maxY = _elements.GetMaxYForFallDown(element, BoardSize);
                    if (maxY != element.NextY)
                    {
                        element.NextY = maxY;
                        GameStateService.Current.StartAnimation();
                    }
                }
            }
        }

        private void DestroyElements()
        {
            foreach (var element in _elements.Where(e => !e.Destroyed))
            {
                CheckForPreviousElementsX(element);
                CheckForPreviousElementsY(element);
            }
            _highScore += _elements.Count(e => e.Destroyed);

            _elements.RemoveAll(e => e.Destroyed);
        }

        private void CheckForPreviousElementsX(Element element)
        {
            var previousElements = _elements.Where(e => element.X - e.X >= 0 && element.X - e.X <= 2 && element.Y == e.Y).ToList();

            if (previousElements.TrueForAll(e => e.Color == element.Color) && previousElements.Count > 2)
            {
                previousElements.ForEach(e => e.Destroyed = true);
            }
        }

        private void CheckForPreviousElementsY(Element element)
        {
            var previousElements = _elements.Where(e => element.Y - e.Y >= 0 && element.Y - e.Y <= 2 && element.X == e.X).ToList();

            if (previousElements.TrueForAll(e => e.Color == element.Color) && previousElements.Count > 2)
            {
                previousElements.ForEach(e => e.Destroyed = true);
            }
        }

        private void SwapElements()
        {
            _selectedElements = _elements.Where(e => e.Selected).ToList();

            if (_selectedElements.Count != 2)
            {
                return;
            }

            // А теперь великолепная проверка на то, что эти элементы находятся рядом
            if (!_selectedElements.ElementsIsNear())
            {
                _selectedElements.ForEach(s => s.Selected = false);
                return;
            }

            _selectedElements[0].NextX = _selectedElements[1].X;
            _selectedElements[0].NextY = _selectedElements[1].Y;
            _selectedElements[1].NextX = _selectedElements[0].X;
            _selectedElements[1].NextY = _selectedElements[0].Y;

            _selectedElements.ForEach(s => s.Selected = false);

            GameStateService.Current.StartAnimation();
        }

        private void SetSelected()
        {
            var mouseState = Mouse.GetState();

            // Нужно найти позицию мыши относительно узлов сетки и нарисовать текстуру курсора
            if (mouseState.X > BoardSize*Element.Size || mouseState.Y > BoardSize*Element.Size)
            {
                return;
            }

            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                _isMousePressed = true;
            }

            if (mouseState.LeftButton == ButtonState.Released && _isMousePressed)
            {
                var x = mouseState.X / Element.Size;
                var y = mouseState.Y / Element.Size;

                if (x < 0 || y < 0)
                {
                    return;
                }

                var element = _elements.GetElement(x, y);
                if (element !=null)
                {
                    element.Selected = !element.Selected;
                }
                _isMousePressed = false;
            }
        }

        protected override void LoadContent()
        {
            _marbleTexture = Game.Content.Load<Texture2D>("Textures/marble");
            _cursorTexture = Game.Content.Load<Texture2D>("Textures/marble_cursor");
            _selectedMarbleTexture = Game.Content.Load<Texture2D>("Textures/marble_glow_1ring");
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            if (GameStateService.Current.GameState != GameState.InGame)
            {
                return;
            }
            base.Draw(gameTime);
            if (_elapsedSeconds < 0.00001)
            {
                GameStateService.Current.ChangeGameState();
                return;
            }

            foreach (var element in _elements)
            {
                element.Draw(_spriteBatch, gameTime);
            }

            DrawCursor();
            DrawHighScore();
            DrawElapsedTime();
        }

        private void DrawElapsedTime()
        {
            _spriteBatch.Begin();

            Font.Draw(_spriteBatch, FontStyle.Large, (BoardSize + 1) * Element.Size, Element.Size, (int)Math.Round(_elapsedSeconds,0));

            _spriteBatch.End();
        }

        private void DrawHighScore()
        {
            _spriteBatch.Begin();

            Font.Draw(_spriteBatch, FontStyle.Large, (BoardSize + 1) * Element.Size, 2 *Element.Size, _highScore);

            _spriteBatch.End();
        }

        private void DrawCursor()
        {
            var mouseCoords = new Vector2();

            if (!SetMouseCoords(ref mouseCoords))
            {
                return;
            }

            _spriteBatch.Begin();
            
            _spriteBatch.Draw(_cursorTexture, mouseCoords, Color.White);

            _spriteBatch.End();
        }

        private bool SetMouseCoords(ref Vector2 vector)
        {
            var mouseState = Mouse.GetState();

            // Нужно найти позицию мыши относительно узлов сетки и нарисовать текстуру курсора
            if (mouseState.X > BoardSize * Element.Size || mouseState.Y > BoardSize * Element.Size)
            {
                return false;
            }

            // Получаем координаты курсора на сетке и рисуем курсор
            var x = mouseState.X / Element.Size;
            var y = mouseState.Y / Element.Size;
            vector.X = x*Element.Size;
            vector.Y = y*Element.Size;

            return true;
        }

        public void StartNewGame()
        {
            _elements.Clear();
            for (int i = 0; i < BoardSize; i++)
            {
                for (int j = 0; j < BoardSize; j++)
                {
                    _elements.Add(new Element(_marbleTexture, _selectedMarbleTexture, i, j, _elementColors[Random.Next(_elementColors.GetLength(0))]));
                }
            }
            _highScore = 0;
            _elapsedSeconds = 60;
        }
    }
}