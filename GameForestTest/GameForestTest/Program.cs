using System;

namespace GameForestTest
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Match3 game = new Match3())
            {
                game.Run();
            }
        }
    }
#endif
}

