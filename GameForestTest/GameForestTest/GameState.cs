﻿namespace GameForestTest
{
    public enum GameState
    {
        MainMenu = 0,

        InGame = 1,

        GameOver = 2
    }
}