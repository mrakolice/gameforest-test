﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace GameForestTest
{
    public static class Extensions
    {
        public static Element GetElement(this IEnumerable<Element> collection, int x, int y)
        {
            return collection.FirstOrDefault(e => e.NextX == x && e.NextY == y);
        }

        public static bool IsElementExist(this IEnumerable<Element> collection, int x, int y)
        {
            return collection.Any(e => e.NextX == x && e.NextY == y);
        }

        public static bool ElementsIsNear(this IEnumerable<Element> collection)
        {
            // Просто бежим по коллекции и для каждого элемента проверяем его "рядочность" с другими элементами
            // Да, мы будем сравнивать одни и те же элемента два раза.
            // Но это же тестовое задание, правда? =)

            var list = collection.ToList();

            // Восхитительная оптимизация от решарпера)
            foreach (Element element in list)
            {
                // X == 0, Y == 2+
                // X == 2+, Y == 0
                // X != 0 && Y != 0
                
                if (list.Any(e => (Math.Abs(element.X - e.X) == 0 && Math.Abs(element.Y - e.Y) > 1) ||
                                  (Math.Abs(element.Y - e.Y) == 0 && Math.Abs(element.X - e.X) > 1)
                                  || (Math.Abs(element.Y - e.Y) > 0 && Math.Abs(element.X - e.X) > 0)
                                  ))
                {
                    return false;
                }
            }
            return true;
        }

        public static int GetMaxYForFallDown(this IEnumerable<Element> collection, Element element, int maxY)
        {
            var elements = collection.ToList();

            var result = element.Y;
            
            for (int i = result+1; i < maxY; i++)
            {
                var otherElement = elements.GetElement(element.X, i);

                if (otherElement == null)
                {
                    result = i;
                }
            }

            return result;
        }


        public static bool CanDestroyElements(this IList<Element> collection, int x, int y, Color color)
        {
            // Беру часть элементов по Х и проверяю, есть ли хотя бы три последовательных с одним цветом
            // Беру часть элементов по Y и проверяю, есть ли хотя бы три последовательных с одним цветом
            return collection.CheckForElementsX(x,y,color) || collection.CheckForElementsY(x,y,color);
        }

        public static bool CheckForElementsX(this IEnumerable<Element> collection, int x, int y, Color color)
        {
            var elements = collection.Where(e => Math.Abs(x - e.X) <= 2 && y == e.Y).OrderBy(e => e.X).ToList();

            // Получили от 3 до 5 блоков
            for (int i = 0; i+2 < 5; i++)
            {
                if (elements.Skip(i).Take(i + 2).All(e => e.Color == color))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool CheckForElementsY(this IEnumerable<Element> collection, int x, int y, Color color)
        {
            var elements = collection.Where(e => Math.Abs(y - e.Y) <= 2 && x == e.X).OrderBy(e => e.Y).ToList();

            // Получили от 3 до 5 блоков
            for (int i = 0; i + 2 < 5; i++)
            {
                if (elements.Skip(i).Take(i + 2).All(e => e.Color == color))
                {
                    return true;
                }
            }
            return false;
        }
    }
}