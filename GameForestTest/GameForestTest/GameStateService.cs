﻿namespace GameForestTest
{
    public class GameStateService
    {
        private GameStateService()
        {
            GameState = GameState.MainMenu;
        }

        private static GameStateService _current;

        public static GameStateService Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new GameStateService();
                }

                return _current;
            }
        }

        public GameState GameState { get; private set; }

        public bool IsAnimated { get; private set; }

        public GameState ChangeGameState ()
        {
            switch (GameState)
            {
               case GameState.MainMenu:
                    GameState = GameState.InGame;
                    break;
                case GameState.InGame:
                    GameState = GameState.GameOver;
                    break;
                case GameState.GameOver:
                    GameState = GameState.MainMenu;
                    break;
            }

            return GameState;
        }

        public void StartAnimation()
        {
            IsAnimated = true;
        }

        public void StopAnimation()
        {
            IsAnimated = false;
        }
    }
}