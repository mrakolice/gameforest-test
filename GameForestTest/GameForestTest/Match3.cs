using System;
using System.Collections.Generic;
using System.Linq;
using Marblets;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameForestTest
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Match3 : Game
    {
        private readonly GraphicsDeviceManager _graphicsDeviceManager;
        private SpriteBatch _spriteBatch;
        private Texture2D _playButton;
        private Texture2D _okButton;
        private Texture2D _gameOver;
        private GameBoard _gameBoard;
        const int MiddleOfBoard = GameBoard.BoardSize / 2 * Element.Size;
        private const int Indentation = 20;

        public Match3()
        {
            _graphicsDeviceManager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _gameBoard = new GameBoard(this, _spriteBatch);
            Components.Add(_gameBoard);


            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            Font.LoadContent(Content);
            _playButton = Content.Load<Texture2D>("Textures/play_button");
            _okButton = Content.Load<Texture2D>("Textures/ok_button");
            _gameOver = Content.Load<Texture2D>("Textures/game_over_frame");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            switch (GameStateService.Current.GameState)
            {
                case GameState.MainMenu:
                    CheckForPlayButton();
                    break;
                case GameState.InGame:
                    base.Update(gameTime);
                    break;
                case GameState.GameOver:
                    CheckForOkButton();
                    break;
            }
        }

        private void CheckForPlayButton()
        {
            var mouseState = Mouse.GetState();

            var differenceX = mouseState.X - MiddleOfBoard;
            var differenceY = mouseState.Y - MiddleOfBoard;

            // ���� ������ ������ Play
            if (differenceX <= _playButton.Width && differenceX >= 0 
                && differenceY <= _playButton.Height && differenceY >= 0

                // � ������ ��������
                && mouseState.LeftButton == ButtonState.Pressed
                )
            {
                // ������ ��������� ���� �� ���������
                GameStateService.Current.ChangeGameState();

                // � ����� ���������� ������������� ������ ����
                _gameBoard.StartNewGame();
            }
        }

        private void CheckForOkButton()
        {
            var mouseState = Mouse.GetState();

            var differenceX = mouseState.X - Element.Size;

            var differenceY = mouseState.Y - Element.Size - _gameOver.Height - Indentation;

            // ���� ������ ������ Ok
            if (differenceX <= _okButton.Width && differenceX >= 0
                && differenceY <= _okButton.Height && differenceY >= 0

                // � ������ ��������
                && mouseState.LeftButton == ButtonState.Pressed)
            {
                // ������ ��������� ���� �� ���������
                GameStateService.Current.ChangeGameState();
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            switch (GameStateService.Current.GameState)
            {
                case GameState.MainMenu:
                    DrawPlayButton();
                    break;
                case GameState.InGame:
                    base.Draw(gameTime);
                    break;
                case GameState.GameOver:
                    DrawGameOver();
                    break;
            }
        }

        private void DrawPlayButton()
        {
            _spriteBatch.Begin();

            _spriteBatch.Draw(_playButton, new Vector2(MiddleOfBoard, MiddleOfBoard), Color.White);

            _spriteBatch.End();
        }

        private void DrawGameOver()
        {
            _spriteBatch.Begin();

            _spriteBatch.Draw(_gameOver, new Vector2(Element.Size, Element.Size), Color.White);
            _spriteBatch.Draw(_okButton, new Vector2(Element.Size, Element.Size + _gameOver.Height + Indentation), Color.White);
            
            _spriteBatch.End();
        }
    }
}
