﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameForestTest
{
    /// <summary>
    /// Базовый элемент для отрисовки
    /// </summary>
    public class Element
    {
        private readonly Texture2D _elementTexture;
        private readonly Texture2D _selectedTexture;
        private int _x;
        private int _y;

        public int X
        {
            get  { return _x; }
            private set
            {
                _x = value;
                NextX = value;
            }
        }

        public int Y
        {
            get { return _y; }
            set { 
                _y = value;
                NextY = value;
            }
        }

        public int NextX { get; set; }
        public int NextY { get; set; }
        public const int AnimationStepsCountPerSecond = 2;
        private double AnimationTime { get; set; }

        public const int Size = 45;

        public bool Selected { get; set; }
        public bool Destroyed { get; set; }

        public Color Color { get; private set; }

        public bool AnimationIsEnded
        {
            get { return NextX == X && NextY == Y; }
        }

        public Element(Texture2D elementTexture, Texture2D selectedTexture, int x, int y, Color color)
        {
            _elementTexture = elementTexture;
            _selectedTexture = selectedTexture;
            X = x;
            Y = y;
            Color = color;
        }

        public void Draw(SpriteBatch batch, GameTime gameTime)
        {
            if (Destroyed)
            {
                return;
            }

            batch.Begin();
            if (NextX != X)
            {
                AnimationTime += gameTime.ElapsedGameTime.TotalSeconds;

                // Нужно вычислить, в каком месте должен находиться спрайт
                if (AnimationTime * AnimationStepsCountPerSecond < 1)
                {
                    var animationX = (float)((X + (NextX - X) * AnimationTime * AnimationStepsCountPerSecond) * Size);
                    batch.Draw(_elementTexture, new Vector2(animationX, Y * Size), Color);
                    if (Selected)
                    {
                        batch.Draw(_selectedTexture, new Vector2(animationX, Y * Size), Color.White);
                    }
                }
                else
                {
                    AnimationTime = 0;
                    X = NextX;
                }
            }
            else if (NextY != Y)
            {
                AnimationTime += gameTime.ElapsedGameTime.TotalSeconds;

                // Нужно вычислить, в каком месте должен находиться спрайт
                if (AnimationTime * AnimationStepsCountPerSecond < 1)
                {
                    var animationY = (float)((Y + (NextY - Y) * AnimationTime * AnimationStepsCountPerSecond) * Size);
                    batch.Draw(_elementTexture, new Vector2(X * Size, animationY), Color);

                    if (Selected)
                    {
                        batch.Draw(_selectedTexture, new Vector2(X * Size, animationY), Color.White);
                    }
                }
                else
                {
                    AnimationTime = 0;
                    Y = NextY;
                }
            }
            else
            {
                batch.Draw(_elementTexture, new Vector2(X * Size, Y * Size), Color);
                if (Selected)
                {
                    batch.Draw(_selectedTexture, new Vector2(X * Size, Y * Size), Color.White);
                }
                AnimationTime = 0;
            }
            
            batch.End();
        }
    }
}